/* eslint-disable react-native/no-inline-styles */
import React, {useState} from 'react';
import {View, TouchableOpacity, ImageBackground, Image} from 'react-native';
import {Container, Content, Text, Footer} from 'native-base';

import ReTextInput from '../../component/ReTextInput';
import ReButton from '../../component/ReButton';
import ReLoadingModal from '../../component/ReLoading';
import ReAlertModal from '../../component/ReAlertModal';
import RePicker from '../../component/RePickerCustom';
import ReModalImagePicker from '../../component/ReModalImagePicker';

import colors from '../../res/colors/index';

import auth from '@react-native-firebase/auth';

import {useNavigation} from '@react-navigation/native';

import {country} from '../../Services/data';

import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

import firestore from '@react-native-firebase/firestore';
import storage from '@react-native-firebase/storage';
import uuid from 'react-native-uuid';

const initialLogin = {
  // nama: 'Bambang',
  // email: 'bambang@gmail.com',
  // hp: '0909809890',
  // alamat: 'tau ah',
  // keperluan: 'kepo',
  // password: 'popopo',
  // username: 'bambang',
  // foto: 'gada',
  // fotoObject: {},

  // ktp: 'gada',
  // ktpObject: {},

  // negara: {value: 'ID', label: 'Indonesia'},
  // gender: { value: 'L', label: 'Laki-Laki'},

  nama: '',
  email: '',
  hp: '',
  alamat: '',
  keperluan: '',
  password: '',
  username: '',
  foto: '',
  fotoObject: {},

  ktp: '',
  ktpObject: {},

  negara: { value: 'ID', label: 'Indonesia' },
  gender: {},
};

const RegisterScreen = () => {
  const navigation = useNavigation();
  const [form, setForm] = useState(initialLogin);
  const [state, setState] = useState({
    loading: false,
    successModal: false,
    visiblePicker: false,
    selectedImage: 'foto',
  });

  const updateFormData = (newData) => {
    setForm((prev) => ({...prev, ...newData}));
  };

  const updateLocalState = (newData) => {
    setState((prev) => ({...prev, ...newData}));
  };

  const togglePicker = () => {
    updateLocalState({visiblePicker: !state.visiblePicker});
  };

  const preparedFoto = async (data) => {
    const uri = data.foto;
    const ext = uri.split('.').pop();
    const filename = `${uuid.v1()}.${ext}`;
    const imageRef = storage().ref(`foto/${filename}`);
    await imageRef.putFile(uri);
    const url = await imageRef.getDownloadURL();

    return url;
  };

  const preparedKtp = async (data) => {
    const uri = data.ktp;
    const ext = uri.split('.').pop();
    const filename = `${uuid.v1()}.${ext}`;
    const imageRef = storage().ref(`foto/${filename}`);
    await imageRef.putFile(uri);
    const url = await imageRef.getDownloadURL();
    return url;
  };

  const preparedData = async () => {
    const formList = form;

    const fotoUri = await preparedFoto(formList);
    const ktpUri = await preparedKtp(formList);

    return {
      nama: formList.nama,
      email: formList.email,
      hp: formList.hp,
      alamat: formList.alamat,
      gender: formList.gender.label,
      negara: formList.negara.label,
      keperluan: formList.keperluan,
      role: 'user',
      foto: fotoUri,
      ktp: ktpUri,
      username: formList.username,
      password: formList.password,
      createAt: firestore.FieldValue.serverTimestamp(),
    };
  };

  const post = firestore().collection('user');

  const onRegister = async () => {
    await updateLocalState({loading: true});
    const formList = await preparedData();

    // console.log('form', formList);
    await post
      .add({
        ...formList,

        // nama: 'Bambang 3',
        // email: 'bambang@gmail.com',
        // hp: '0909809890',
        // alamat: 'tau ah',
        // keperluan: 'kepo',
        // password: 'popopo',
        // username: 'bambang',
        // foto: 'gada',
        // ktp: 'gada',
        // negara: 'Indonesia',
        // gender: 'Laki-Laki',
      })
      .then(() => {
        alert('Berhasil menyimpan data');
        navigation.navigate('LoginScreen');
      })
      .catch((e) => {
        alert('Gagal menyimpan data');
      });

    await updateLocalState({loading: false});
  };

  const checkFormRegister = () => {
    if (
      form.nama.length > 0 &&
      form.email.length > 0 &&
      form.hp.length > 0 &&
      form.alamat.length > 0 &&
      Object.keys(form.gender).length > 0 &&
      Object.keys(form.negara).length > 0 &&
      form.keperluan.length > 0 &&
      form.password.length > 0 &&
      form.username.length > 0
    ) {
      return false;
    }
    return true;
  };

  return (
    <Container
      style={{
        flex: 1,
        backgroundColor: colors.white,
      }}>
      <ImageBackground
        style={{flex: 1}}
        source={require('../../res/image/bg.jpeg')}>
        <View
          style={{
            marginVertical: wp(2),
            alignSelf: 'center',
            // marginLeft: wp(5),
            marginBottom: wp(4),
          }}>
          <Text
            style={{
              fontSize: wp(6.5),
              fontWeight: 'bold',
              color: colors.white,
            }}>
            Register
          </Text>
        </View>
        <Content>
          <View
            style={{
              // flex: 1,
              alignItems: 'center',
              // justifyContent: 'center',
            }}>
            <View style={{flexDirection: 'row', marginBottom: wp(2)}}>
              <View style={{marginVertical: hp(1.2), marginHorizontal: wp(8)}}>
                <TouchableOpacity
                  onPress={() => {
                    togglePicker();
                    updateLocalState({
                      selectedImage: 'foto',
                    });
                  }}
                  style={{justifyContent: 'center', alignItems: 'center'}}
                  transparent>
                  {form.foto.length > 0 ? (
                    <Image
                      source={{
                        uri: form.foto,
                      }}
                      style={{
                        height: hp(10),
                        width: hp(10),
                        borderRadius: hp(20),
                        resizeMode: 'cover',
                      }}
                    />
                  ) : (
                    <Image
                      source={require('../../res/image/Profile.png')}
                      style={{
                        height: hp(10),
                        width: hp(10),
                        borderRadius: hp('20%'),
                        resizeMode: 'cover',
                      }}
                    />
                  )}
                </TouchableOpacity>
                <View style={{alignItems: 'center'}}>
                  <Text style={{fontSize: wp(3)}}>Foto Profile</Text>
                </View>
              </View>
              <View style={{marginVertical: hp(1.2), marginHorizontal: wp(8)}}>
                <TouchableOpacity
                  onPress={() => {
                    togglePicker();
                    updateLocalState({
                      selectedImage: 'ktp',
                    });
                  }}
                  style={{justifyContent: 'center', alignItems: 'center'}}
                  transparent>
                  {form.ktp.length > 0 ? (
                    <Image
                      source={{
                        uri: form.ktp,
                      }}
                      style={{
                        height: hp(10),
                        width: hp(10),
                        borderRadius: hp(20),
                        resizeMode: 'cover',
                      }}
                    />
                  ) : (
                    <Image
                      source={require('../../res/image/credit-card.png')}
                      style={{
                        height: hp(10),
                        width: hp(10),
                        // borderRadius: hp('20%'),
                        resizeMode: 'cover',
                      }}
                    />
                  )}
                </TouchableOpacity>
                <View style={{alignItems: 'center'}}>
                  <Text style={{fontSize: wp(3)}}>Foto KTP</Text>
                </View>
              </View>
            </View>

            <View style={{marginVertical: wp(2), width: wp(85)}}>
              <ReTextInput
                label="Nama"
                value={form.nama}
                onChangeText={(val) => {
                  setForm({...form, nama: val});
                }}
              />
            </View>
            <View style={{marginVertical: wp(2), width: wp(85)}}>
              <ReTextInput
                label="Email"
                value={form.email}
                onChangeText={(val) => {
                  setForm({...form, email: val});
                }}
              />
            </View>
            <View style={{marginVertical: wp(2), width: wp(85)}}>
              <ReTextInput
                label="No. HP"
                numeric
                value={form.hp}
                onChangeText={(val) => {
                  setForm({...form, hp: val});
                }}
              />
            </View>
            <View style={{marginVertical: wp(2), width: wp(85)}}>
              <ReTextInput
                label="Alamat"
                value={form.alamat}
                onChangeText={(val) => {
                  setForm({...form, alamat: val});
                }}
              />
            </View>
            <View style={{marginVertical: wp(2), width: wp(85)}}>
              <RePicker
                label="Jenis Kelamin"
                data={[
                  {value: 'L', label: 'Laki-Laki'},
                  {value: 'L', label: 'Perempuan'},
                ]}
                value={form.gender}
                onValueChange={(value) => {
                  setForm({...form, gender: value});

                  console.log('val', value);
                }}
              />
            </View>

            <View style={{marginVertical: wp(2), width: wp(85)}}>
              <RePicker
                label="Negara"
                data={country}
                value={form.negara}
                onValueChange={(value) => {
                  setForm({...form, negara: value});
                }}
              />
            </View>
            <View style={{marginVertical: wp(2), width: wp(85)}}>
              <ReTextInput
                label="Keperluan Kedatangan"
                value={form.keperluan}
                onChangeText={(val) => {
                  setForm({...form, keperluan: val});
                }}
              />
            </View>
            <View style={{marginVertical: wp(2), width: wp(85)}}>
              <ReTextInput
                label="Username"
                value={form.username}
                onChangeText={(val) => {
                  setForm({...form, username: val});
                }}
              />
            </View>
            <View style={{marginVertical: wp(2), width: wp(85)}}>
              <ReTextInput
                label="Password"
                value={form.password}
                onChangeText={(val) => {
                  setForm({...form, password: val});
                }}
                passwordInput
              />
            </View>
            <View
              style={{marginVertical: wp(2), marginTop: wp(4), width: wp(85)}}>
              <ReButton
                label="Register"
                disabled={checkFormRegister()}
                onPress={() => {
                  onRegister();
                }}
              />
            </View>
            <View
              style={{
                marginTop: wp(2),
                justifyContent: 'center',
                alignItems: 'center',
                flexDirection: 'row',
              }}>
              <Text style={{color: colors.white}}>
                {'Sudah Punya Akun ?  '}
              </Text>
              <TouchableOpacity
                onPress={() => navigation.navigate('LoginScreen')}>
                <Text style={{color: colors.white, fontWeight: 'bold'}}>
                  Login
                </Text>
              </TouchableOpacity>
            </View>
          </View>
          <View style={{height: hp(5)}} />
        </Content>
      </ImageBackground>

      <ReLoadingModal visible={state.loading} title="Register .." />
      <ReAlertModal
        visible={state.successModal}
        title="Berhasil"
        subTitle="Berhasil Mendaftar"
        onConfirm={() => {
          setState({...state, successModal: false});
          navigation.navigate('LoginScreen');
        }}
      />
      <ReModalImagePicker
        visible={state.visiblePicker}
        onCancel={() => togglePicker()}
        onPickImage={(image) => {
          togglePicker();
          if (state.selectedImage === 'foto') {
            updateFormData({
              fotoObject: image,
              foto: image.uri,
            });
          } else {
            updateFormData({
              ktpObject: image,
              ktp: image.uri,
            });
          }
        }}
      />
    </Container>
  );
};

export default RegisterScreen;
