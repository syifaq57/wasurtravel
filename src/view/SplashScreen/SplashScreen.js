/* eslint-disable no-alert */
/* eslint-disable react-native/no-inline-styles */
/* eslint-disable react-hooks/exhaustive-deps */
import React, {useState, useEffect} from 'react';
import {StyleSheet, AsyncStorage, Alert, ActivityIndicator} from 'react-native';
import {Container, View, Text} from 'native-base';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

import firestore from '@react-native-firebase/firestore';

const initialLogin = {
  username: '',
  password: '',
};

const SplashScreen = ({navigation}) => {
  const [, setLoginForm] = useState(initialLogin);

  const LoginUser = async (username, password) => {
    // await setLoading(true);
    let User;

    await firestore()
      .collection('user')
      .where('username', '==', username)
      .where('password', '==', password)
      .onSnapshot(async (snapshot) => {
        await snapshot.forEach((doc) => {
          if (doc) {
            User = {
              id: doc.id,
              nama: doc.data().nama,
              email: doc.data().email,
              role: doc.data().role,
              hp: doc.data().hp,
              alamat: doc.data().alamat,
              gender: doc.data().gender,
              negara: doc.data().negara,
              keperluan: doc.data().keperluan,
              foto: doc.data().foto,
              ktp: doc.data().ktp,
              username: doc.data().username,
              password: doc.data().password,
              createAt: doc.data().createAt,
            };
          }
        });

        if (User) {
          AsyncStorage.setItem('userData', JSON.stringify(User));
          setLoginForm(initialLogin);
          if (User.role === 'user') {
            await postAntrian(User.id);
            navigation.navigate('HomeUser');
          } else if (User.role === 'admin') {
            await getAntrian();
            navigation.navigate('HomeAdmin');
          } else {
            alert('Gagal Login');
          }
        } else {
          Alert.alert('Gagal Login', 'Username / Password tidak sesuai');
        }
        // setLoading(false);
      });
  };

  const antrian = firestore().collection('antrian');

  const getAntrian = async () => {
    const timeNow = await getDateNow();
    await antrian
      .where('createAt', '==', timeNow)
      // .where('user', '==', user_id)
      .get()
      .then(async (snapshot) => {
        const docLength = snapshot.docs.length;
        AsyncStorage.setItem('antrian', docLength.toString());
      });
  };

  const postAntrian = async (userid) => {
    const timeNow = await getDateNow();

    await antrian
      .where('createAt', '==', timeNow)
      // .where('user', '==', user_id)
      .get()
      .then(async (snapshot) => {
        let newAntrian = [];
        let itemAntrian;
        const docLength = snapshot.docs.length;
        if (docLength > 0) {
          await snapshot.docs.forEach(async (doc) => {
            const newData = await doc.data();
            newAntrian.push(newData);
          });

          itemAntrian = {
            antrian: docLength + 1,
            createAt: timeNow,
            user: firestore().collection('user').doc(userid),
          };

          const newList = await newAntrian.find((v) => v.user.id === userid);

          if (!newList) {
            antrian.add({...itemAntrian});
          } else {
            itemAntrian = newList;
          }
        } else {
          itemAntrian = {
            antrian: 1,
            createAt: timeNow,
            user: firestore().collection('user').doc(userid),
          };

          antrian.add({...itemAntrian});
        }

        AsyncStorage.setItem('antrian', itemAntrian.antrian.toString());
      });
  };

  const getDateNow = () => {
    var currentdate = new Date();
    let start = new Date(currentdate.toLocaleDateString());

    // const value = start.getTime()
    return start.getTime();
  };

  useEffect(() => {
    AsyncStorage.getItem('userData').then((item) => {
      const user = JSON.parse(item);
      if (user && Object.keys(user).length > 0) {
        LoginUser(user.username, user.password);
      } else {
        setTimeout(() => {
          navigation.navigate('LoginScreen');
        }, 3000);
      }
    });
  }, []);

  return (
    <Container>
      <View
        style={{
          backgroundColor: 'black',
          flex: 1,
          justifyContent: 'center',
          padding: 20,
        }}>
        <View style={stylePage.viewPT}>
          <View style={stylePage.logoView}>
            <Text style={{color: 'white', fontSize: wp(5), fontWeight: 'bold'}}>
              Wasur App
            </Text>
          </View>
          <View style={stylePage.logoView}>
            <View style={{paddingVertical: hp('2%')}}>
              <ActivityIndicator size={'large'} color="white" />
            </View>
          </View>
          {/* <View style={stylePage.logoView}>
            <Text style={{fontSize: wp('4.5%'), fontWeight: 'bold'}}>
              Loading . . . .
            </Text>
          </View> */}
        </View>
      </View>
    </Container>
  );
};

const stylePage = StyleSheet.create({
  viewPT: {
    marginHorizontal: wp('5%'),
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: -15,
  },
  logoView: {
    alignItems: 'center',
  },
  logo: {
    width: hp('20%'),
    height: hp('20%'),
    resizeMode: 'contain',
    // borderRadius: wp(30),
  },
  loading: {
    width: hp('10%'),
    height: hp('10%'),
    resizeMode: 'contain',
  },
});

export default SplashScreen;
