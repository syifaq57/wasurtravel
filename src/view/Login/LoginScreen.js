/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable react-native/no-inline-styles */
import React, {useState, useEffect} from 'react';
import {
  View,
  TouchableOpacity,
  AsyncStorage,
  ImageBackground,
  Keyboard,
  Alert,
} from 'react-native';
import {Container, Text} from 'native-base';

import ReTextInput from '../../component/ReTextInput';
import ReButton from '../../component/ReButton';
import ReLoadingModal from '../../component/ReLoading';
import ReAlertModal from '../../component/ReAlertModal';
import colors from '../../res/colors/index';

import {useNavigation} from '@react-navigation/native';
import firestore from '@react-native-firebase/firestore';

import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

const initialLogin = {
  username: '',
  password: '',
};

const LoginScreen = () => {
  const [loginForm, setLoginForm] = useState(initialLogin);
  const [state, setState] = useState({
    loading: false,
    successModal: false,

    titleDisplay: true,
  });

  const [loading, setLoading] = useState(false);
  const navigation = useNavigation();

  const updateState = (newData) => {
    setState((prev) => ({...prev, ...newData}));
  };

  const LoginUser = async () => {
    await setLoading(true);
    let User;

    await firestore()
      .collection('user')
      .where('username', '==', loginForm.username)
      .where('password', '==', loginForm.password)
      .onSnapshot(async (snapshot) => {
        await snapshot.forEach((doc) => {
          if (doc) {
            User = {
              id: doc.id,
              nama: doc.data().nama,
              email: doc.data().email,
              role: doc.data().role,
              hp: doc.data().hp,
              alamat: doc.data().alamat,
              gender: doc.data().gender,
              negara: doc.data().negara,
              keperluan: doc.data().keperluan,
              foto: doc.data().foto,
              ktp: doc.data().ktp,
              username: doc.data().username,
              password: doc.data().password,
              createAt: doc.data().createAt,
            };
          }
        });

        if (User) {
          AsyncStorage.setItem('userData', JSON.stringify(User));
          setLoginForm(initialLogin);
          if (User.role === 'user') {
            await postAntrian(User.id);
            navigation.navigate('HomeUser');
          } else if (User.role === 'admin') {
            await getAntrian();
            navigation.navigate('HomeAdmin');
          } else {
            // alert('Gagal Login');
            console.log('gagal');
          }
        } else {
          Alert.alert('Gagal Login', 'Username / Password tidak sesuai');
        }
        setLoading(false);
      });
  };

  const antrian = firestore().collection('antrian');

  const getAntrian = async () => {
    const timeNow = await getDateNow();
    await antrian
      .where('createAt', '==', timeNow)
      // .where('user', '==', user_id)
      .get()
      .then(async (snapshot) => {
        const docLength = snapshot.docs.length;
        AsyncStorage.setItem('antrian', docLength.toString());
      });
  };

  const postAntrian = async (userid) => {
    const timeNow = await getDateNow();

    await antrian
      .where('createAt', '==', timeNow)
      // .where('user', '==', user_id)
      .get()
      .then(async (snapshot) => {
        let newAntrian = [];
        let itemAntrian;
        const docLength = snapshot.docs.length;
        if (docLength > 0) {
          await snapshot.docs.forEach(async (doc) => {
            const newData = await doc.data();
            newAntrian.push(newData);
          });

          itemAntrian = {
            antrian: docLength + 1,
            createAt: timeNow,
            user: firestore().collection('user').doc(userid),
          };

          const newList = await newAntrian.find((v) => v.user.id === userid);

          if (!newList) {
            antrian.add({...itemAntrian});
          } else {
            itemAntrian = newList;
          }
        } else {
          itemAntrian = {
            antrian: 1,
            createAt: timeNow,
            user: firestore().collection('user').doc(userid),
          };

          antrian.add({...itemAntrian});
        }

        AsyncStorage.setItem('antrian', itemAntrian.antrian.toString());
      });
  };

  const getDateNow = () => {
    var currentdate = new Date();
    let start = new Date(currentdate.toLocaleDateString());

    // const value = start.getTime()
    return start.getTime();
  };

  const checkFormLogin = () => {
    if (loginForm.username.length > 0 && loginForm.password.length > 0) {
      return false;
    }
    return true;
  };

  // useEffect(() => {
  //   AsyncStorage.getItem('loginForm').then((item) => {
  //     const parse = JSON.parse(item);
  //     if (parse && Object.keys(parse).length > 0) {
  //       setLoginForm({
  //         email: parse.email,
  //         password: parse.password,
  //       });
  //     }
  //   });
  // }, []);

  const onKeyboardDidShow = () => {
    updateState({titleDisplay: false});
  };

  const onKeyboardDidHide = () => {
    updateState({titleDisplay: true});
  };

  useEffect(() => {
    Keyboard.addListener('keyboardDidShow', onKeyboardDidShow);
    Keyboard.addListener('keyboardDidHide', onKeyboardDidHide);

    // cleanup function
    return () => {
      Keyboard.removeListener('keyboardDidShow', onKeyboardDidShow);
      Keyboard.removeListener('keyboardDidHide', onKeyboardDidHide);
    };
  }, []);

  return (
    <Container
      style={{
        flex: 1,
        backgroundColor: colors.white,
      }}>
      <ImageBackground
        style={{flex: 1}}
        source={require('../../res/image/bg.jpeg')}>
        <View
          style={{
            flex: 1,
            alignItems: 'center',
            justifyContent: 'center',
            paddingHorizontal: wp(2),
            marginTop: state.titleDisplay ? hp(-20) : 0,
          }}>
          {state.titleDisplay ? (
            <View style={{alignItems: 'center', marginBottom: wp(10)}}>
              <View style={{marginBottom: wp(4), alignItems: 'center'}}>
                <Text
                  style={{
                    fontSize: wp(5.5),
                    fontWeight: 'bold',
                    color: colors.white,
                  }}>
                  Selamat Datang Di Wasur
                </Text>
                <Text
                  style={{
                    fontSize: wp(5.5),
                    fontWeight: 'bold',
                    color: colors.white,
                  }}>
                  Silahkan Login Terlebih Dahulu
                </Text>
              </View>
            </View>
          ) : null}

          <View style={{marginVertical: wp(2), width: wp(90)}}>
            <ReTextInput
              label="Username"
              value={loginForm.username}
              onChangeText={(val) => {
                setLoginForm({...loginForm, username: val});
              }}
            />
          </View>
          <View style={{marginVertical: wp(2), width: wp(90)}}>
            <ReTextInput
              label="Password"
              value={loginForm.password}
              onChangeText={(val) => {
                setLoginForm({...loginForm, password: val});
              }}
              passwordInput
            />
          </View>
          <View style={{marginVertical: wp(2), width: wp(90)}}>
            <ReButton
              label="LOGIN"
              disabled={checkFormLogin()}
              onPress={() => {
                LoginUser();
              }}
            />
          </View>
          <View
            style={{
              marginTop: wp(2),
              justifyContent: 'center',
              alignItems: 'center',
              flexDirection: 'row',
            }}>
            <Text style={{color: colors.white}}>{'Belum Punya Akun ?  '}</Text>
            <TouchableOpacity
              onPress={() => navigation.navigate('RegisterScreen')}>
              <Text style={{color: colors.white, fontWeight: 'bold'}}>
                Daftar
              </Text>
            </TouchableOpacity>
          </View>
        </View>
        <View
          style={{
            position: 'absolute',
            bottom: 10,
            alignItems: 'center',
            width: '100%',
          }}>
          <Text style={{fontWeight: 'bold'}}>v.1.2.1</Text>
        </View>
      </ImageBackground>

      <ReLoadingModal visible={loading} title="Login.." />
      <ReAlertModal
        visible={state.successModal}
        title="Gagal Login"
        subTitle="Cek Email dan Password"
        onConfirm={() => {
          setState({...state, successModal: false});
        }}
      />
      {/* <Footer
        style={{
          backgroundColor: colors.white,
          borderTopColor: colors.gray05,
          borderWidth: 0.5,
        }}>
        <View
          style={{
            justifyContent: 'center',
            alignItems: 'center',
            flexDirection: 'row',
          }}>
          <Text style={{color: colors.gray02}}>
            {'Dont have an account ?  '}
          </Text>
          <TouchableOpacity
            onPress={() => navigation.navigate('RegisterScreen')}>
            <Text style={{color: colors.gray02, fontWeight: 'bold'}}>
              Sign Up
            </Text>
          </TouchableOpacity>
        </View>
      </Footer> */}
    </Container>
  );
};

export default LoginScreen;
