/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable react-native/no-inline-styles */
import React, {useState, useEffect} from 'react';
import {View, Image, TouchableOpacity, Alert} from 'react-native';
import {Container, Content, Text} from 'native-base';
import DeafaultHeader from '../../component/DeafaultHeader';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import colors from '../../res/colors/index';
import {useNavigation, useRoute} from '@react-navigation/native';

import FontAwesome from 'react-native-vector-icons/FontAwesome';

import firestore from '@react-native-firebase/firestore';
import ReLoadingModal from '../../component/ReLoading';

const UserList = () => {
  const navigation = useNavigation();
  const route = useRoute();

  const [state, setState] = useState({
    loading: false,
    disableForm: false,
    loadingPopup: false,
    disableEdit: false,
  });

  const [userlist, setUserlist] = useState([]);

  const updateLocalState = (newData) => {
    setState((prev) => ({...prev, ...newData}));
  };

  const getDateNow = () => {
    var currentdate = new Date();
    let start = new Date(currentdate.toLocaleDateString());

    // const value = start.getTime()
    return start.getTime();
  };

  const post = firestore().collection('user');

  const loadUserList = async () => {
    await updateLocalState({loading: true});
    const userlist = await firestore().collection('user');
    await userlist.orderBy('createAt', 'desc');
    await userlist.onSnapshot((snapshot) => {
      let list = [];
      snapshot.forEach((doc) => {
        list.push({
          id: doc.id,
          nama: doc.data().nama,
          email: doc.data().email,
          hp: doc.data().hp,
          alamat: doc.data().alamat,
          gender: doc.data().gender,
          negara: doc.data().negara,
          keperluan: doc.data().keperluan,
          role: doc.data().role,
          foto: doc.data().foto,
          ktp: doc.data().ktp,
          username: doc.data().username,
          password: doc.data().password,
        });
      });
      setUserlist(list);

      // console.log('snapshot', list);
    });

    await updateLocalState({loading: false});
  };

  const onDeleteUser = async (userid) => {
    await updateLocalState({loadingPopup: true});
    await post.doc(userid).delete();

    await updateLocalState({loadingPopup: false});
  };

  const deleteUser = (user) => {
    Alert.alert('Hapus User!', 'Apakah Kamu yakin ingin Hapus user ini ?', [
      {
        text: 'Cancel',
        onPress: () => null,
        style: 'cancel',
      },
      {text: 'YES', onPress: () => onDeleteUser(user)},
    ]);
  };

  const editUser = (user) => {
    navigation.navigate('UserEdit', {user: user});
  };

  const onGetAntrianToday = async () => {
    const antrian = await firestore().collection('antrian');
    let newAntrian = [];
    const time = await getDateNow();
    // console.log('time', time);
    await antrian
      .where('createAt', '==', time)
      .get()
      .then(async (snapshot) => {
        const docLength = await snapshot.docs.length;

        // console.log('snapshot', docLength);

        if (docLength > 0) {
          // console.log('snapp', snapshot.docs)
          for (const doc of snapshot.docs) {
            const newData = await doc.data();
            newData.user = await newData.user.get().then((us) => {
              return us.data();
            });
            await newAntrian.push(newData);
          }
        } else {
          alert('Data Tidak Tersedia');
        }
      });

    // console.log('tess', state.antrianList);
    return await newAntrian;
  };

  const checkParams = async () => {
    const params = route.params;
    if (params) {
      const itemuser = await onGetAntrianToday();
      const list = [];
      await itemuser.forEach((item) => {
        list.push({
          nama: item.user.nama,
          email: item.user.email,
          keperluan: item.user.keperluan,
        })
      })
      updateLocalState({disableEdit: true})
      // console.log('prm', list)
      setUserlist(list);
    } else {
      loadUserList();
    }
  }

  useEffect(() => {
    checkParams();
  }, []);

  return (
    <Container
      style={{
        flex: 1,
        backgroundColor: colors.white,
      }}>
      <Image
        source={require('../../res/image/background.jpg')}
        resizeMode="contain"
        style={{position: 'absolute'}}
      />
      <DeafaultHeader backButton title="Daftar User" />
      <Content
        style={{
          paddingHorizontal: wp(5),
          paddingTop: hp(2),
        }}>
        {userlist.map((user, index) => (
          <View
            key={index}
            style={{
              marginVertical: wp(2),
              backgroundColor: 'rgba(255,255,255,0.7)',
              borderRadius: wp(2),
              paddingHorizontal: wp(4),
              paddingVertical: wp(2),
            }}>
            {!state.disableEdit ? (
              <View
              style={{
                flexDirection: 'row',
                position: 'absolute',
                right: wp(1),
                top: wp(2),
              }}>
              <TouchableOpacity
                onPress={() => {
                  deleteUser(user.id);
                }}
                style={{marginHorizontal: wp(2), zIndex: 1}}>
                <FontAwesome name="trash" color={colors.warning} size={wp(5)} />
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => {
                  editUser(user);
                }}
                style={{marginHorizontal: wp(2), zIndex: 1}}>
                <FontAwesome
                  name="pencil"
                  color={colors.green02}
                  size={wp(5)}
                />
              </TouchableOpacity>
            </View>
            ) : null}

            <View style={{flexDirection: 'row', alignItems: 'center'}}>
              <View style={{marginRight: wp(3.5)}}>
                <Image
                  source={require('../../res/image/Profile.png')}
                  style={{
                    height: hp('7%'),
                    width: hp('7%'),
                    borderRadius: hp('20%'),
                    resizeMode: 'cover',
                  }}
                />
              </View>
              <View style={{flex: 1}}>
                <View style={{marginVertical: wp(1)}}>
                  <Text style={{fontSize: wp(2.5)}}>Nama</Text>
                  <Text
                    numberOfLines={1}
                    ellipsizeMode="tail"
                    style={{fontSize: wp(3.5), fontWeight: 'bold'}}>
                    {user.nama}
                  </Text>
                </View>
                <View style={{marginVertical: wp(1)}}>
                  <Text style={{fontSize: wp(2.5)}}>Email</Text>
                  <Text
                    numberOfLines={1}
                    ellipsizeMode="tail"
                    style={{fontSize: wp(3.5), fontWeight: 'bold'}}>
                    {user.email}
                  </Text>
                </View>
                {state.disableEdit ? (
                <View style={{marginVertical: wp(1)}}>
                  <Text style={{fontSize: wp(2.5)}}>Keperluan</Text>
                  <Text
                    numberOfLines={1}
                    ellipsizeMode="tail"
                    style={{fontSize: wp(3.5), fontWeight: 'bold'}}>
                    {user.keperluan}
                  </Text>
                </View>
                ) : null}
              </View>
            </View>
          </View>
        ))}

        <ReLoadingModal visible={state.loading} title="Load Data" />
        <ReLoadingModal visible={state.loadingPopup} title="Save Data" />

        <View style={{height: wp(10)}} />
      </Content>
    </Container>
  );
};

export default UserList;
