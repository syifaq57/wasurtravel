/* eslint-disable no-alert */
/* eslint-disable react-native/no-inline-styles */
import React, {useState} from 'react';
import {View, Image} from 'react-native';
import {Container, Content, Text} from 'native-base';
import DeafaultHeader from '../../component/DeafaultHeader';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import colors from '../../res/colors/index';

import FontAwesome from 'react-native-vector-icons/FontAwesome';

import ReTextInput from '../../component/ReTextInput';
import ReButton from '../../component/ReButton';
import {TouchableOpacity} from 'react-native-gesture-handler';
import ReTextArea from '../../component/ReTextArea';
import ReModalImagePicker from '../../component/ReModalImagePicker';

import firestore from '@react-native-firebase/firestore';
import storage from '@react-native-firebase/storage';
import uuid from 'react-native-uuid';
import ReLoadingModal from '../../component/ReLoading';

const initialForm = {
  judul: '',
  deskripsi: '',
  foto: '',
  fotoObject: {},
};

const TambahFasilitas = () => {
  const [state, setState] = useState({
    visiblePicker: false,
    loading: false,
  });
  const [form, setForm] = useState(initialForm);

  const updateForm = (newData) => {
    setForm((prev) => ({
      ...prev,
      ...newData,
    }));
  };

  const updateLocalState = (newData) => {
    setState((prev) => ({...prev, ...newData}));
  };

  const togglePicker = () => {
    updateLocalState({visiblePicker: !state.visiblePicker});
  };

  const checkForm = () => {
    if (
      form.judul.length > 0 &&
      form.deskripsi.length > 0 &&
      form.foto.length > 0 &&
      Object.keys(form.fotoObject).length > 0
    ) {
      return false;
    }
    return true;
  };

  const preparedFoto = async (data) => {
    const uri = data.foto;
    const ext = uri.split('.').pop();
    const filename = `${uuid.v1()}.${ext}`;
    const imageRef = storage().ref(`foto/${filename}`);
    await imageRef.putFile(uri);
    const url = await imageRef.getDownloadURL();

    return url;
  };

  const preparedData = async () => {
    const formList = form;

    const fotoUri = await preparedFoto(formList);

    return {
      judul: formList.judul,
      deskripsi: formList.deskripsi,
      foto: fotoUri,
      createAt: firestore.FieldValue.serverTimestamp(),
    };
  };

  const post = firestore().collection('fasilitas');

  const onSaveFasilitas = async () => {
    await updateLocalState({loading: true});

    const newData = await preparedData();

    console.log('form', newData);

    await post
      .add({
        ...newData,
      })
      .then(() => {
        alert('Berhasil menyimpan data');
        // navigation.navigate('LoginScreen');
        setForm(initialForm);
      })
      .catch(() => {
        alert('Gagal menyimpan data');
      });

    await updateLocalState({loading: false});
  };

  return (
    <Container
      style={{
        flex: 1,
        backgroundColor: colors.white,
      }}>
      <Image
        source={require('../../res/image/background.jpg')}
        resizeMode="contain"
        style={{position: 'absolute'}}
      />
      <DeafaultHeader backButton title="Tambah Fasilitas" />
      <Content
        style={{
          paddingHorizontal: wp(5),
          paddingTop: hp(2),
        }}>
        <View style={{marginVertical: wp(2), width: wp(90)}}>
          <ReTextInput
            label="Judul"
            labelColor={colors.gray11}
            value={form.judul}
            valueColor={colors.green02}
            onChangeText={(val) => {
              updateForm({judul: val});
            }}
          />
        </View>
        <View style={{marginVertical: wp(2), width: wp(90)}}>
          <ReTextArea
            label="Deskripsi"
            labelColor={colors.gray11}
            value={form.deskripsi}
            valueColor={colors.green02}
            multiLine
            onChangeText={(val) => {
              updateForm({deskripsi: val});
            }}
          />
        </View>
        <View style={{marginVertical: wp(2), width: wp(90)}}>
          <Text
            style={{fontSize: wp(4), fontWeight: 'bold', marginBottom: hp(1)}}>
            Tambah Foto
          </Text>
          <TouchableOpacity
            onPress={() => {
              togglePicker();
            }}
            style={{
              height: hp(24),
              backgroundColor: 'rgba(255,255,255,0.8)',
              borderRadius: wp(1),
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            {form.foto.length > 0 ? (
              <Image
                source={{
                  uri: form.foto,
                }}
                style={{
                  height: hp(22),
                  width: hp(100),
                  // borderRadius: hp(20),
                  resizeMode: 'contain',
                }}
              />
            ) : (
              <FontAwesome name="camera" color={colors.black} size={wp(8)} />
            )}
          </TouchableOpacity>
        </View>
        <View style={{marginVertical: wp(2), marginTop: wp(4), width: wp(90)}}>
          <ReButton
            label="Simpan"
            disabled={checkForm()}
            onPress={() => {
              onSaveFasilitas();
            }}
          />
        </View>
        <ReModalImagePicker
          visible={state.visiblePicker}
          onCancel={() => togglePicker()}
          onPickImage={(image) => {
            console.log(image);
            togglePicker();
            updateForm({
              fotoObject: image,
              foto: image.uri,
            });
          }}
        />
        <ReLoadingModal
          visible={state.loading}
          title="Menambahkan Fasilitas .."
        />

        <View style={{height: wp(10)}} />
      </Content>
    </Container>
  );
};

export default TambahFasilitas;
