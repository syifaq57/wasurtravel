/* eslint-disable no-alert */
/* eslint-disable react-native/no-inline-styles */
import React, {useState} from 'react';
import {View, Image, PermissionsAndroid, Alert} from 'react-native';
import {Container, Content} from 'native-base';
import DeafaultHeader from '../../component/DeafaultHeader';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import colors from '../../res/colors/index';

import ReButton from '../../component/ReButton';

import firestore from '@react-native-firebase/firestore';
import ReLoadingModal from '../../component/ReLoading';
import RePicker from '../../component/RePicker';
import ReDatePicker from '../../component/ReDatePicker';

import RNFS, {writeFile} from 'react-native-fs';

import XLSX from 'xlsx';

const initialForm = {
  reportType: '',
  date: '',
  bulan: '',
  tahun: '',
};

const bulan = [
  {value: '01', label: 'Januari'},
  {value: '02', label: 'Februari'},
  {value: '03', label: 'Maret'},
  {value: '04', label: 'April'},
  {value: '05', label: 'Mei'},
  {value: '06', label: 'Juni'},
  {value: '07', label: 'Juli'},
  {value: '08', label: 'Agustus'},
  {value: '09', label: 'September'},
  {value: '10', label: 'Oktober'},
  {value: '11', label: 'November'},
  {value: '12', label: 'Desember'},
];

const year = [
  {value: '2019', label: '2019'},
  {value: '2020', label: '2020'},
  {value: '2021', label: '2021'},
  {value: '2022', label: '2022'},
  {value: '2023', label: '2019'},
  {value: '2024', label: '2025'},
];

const ReportPage = () => {
  const [state, setState] = useState({
    visiblePicker: false,
    loading: false,
    antrianList: [],
  });
  const [form, setForm] = useState(initialForm);

  const updateForm = (newData) => {
    setForm((prev) => ({
      ...prev,
      ...newData,
    }));
  };

  const normalizeDate = (date) => {
    var currentdate = new Date(date);
    let start = new Date(currentdate.toLocaleDateString());

    // const value = start.getTime()
    return start.getTime();
  };

  // var data = [
  //   {name: 'John', city: 'Seattle'},
  //   {name: 'Mike', city: 'Los Angeles'},
  //   {name: 'Zach', city: 'New York'},
  // ];

  const exportExcel = async (data, date) => {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
        {
          title: 'Cool Photo App Camera Permission',
          message:
            'Cool Photo App needs access to your camera ' +
            'so you can take awesome pictures.',
          buttonNeutral: 'Ask Me Later',
          buttonNegative: 'Cancel',
          buttonPositive: 'OK',
        },
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        // console.log('You can use the camera');
        const ws = await XLSX.utils.json_to_sheet(data);

        // ws.A1.s = await {
        //   fill: {
        //     patternType: 'none', // none / solid
        //     fgColor: {rgb: 'FF000000'},
        //     bgColor: {rgb: 'FFFFFFFF'},
        //   },
        //   font: {
        //     name: 'Times New Roman',
        //     sz: 16,
        //     color: {rgb: '#FF0'},
        //     bold: true,
        //     italic: false,
        //     underline: false,
        //   },
        //   border: {
        //     top: {style: 'thin', color: {auto: 1}},
        //     right: {style: 'thin', color: {auto: 1}},
        //     bottom: {style: 'thin', color: {auto: 1}},
        //     left: {style: 'thin', color: {auto: 1}},
        //   },
        // };
        // console.log('ws', ws);
        /* build new workbook */
        const wb = await XLSX.utils.book_new();
        await XLSX.utils.book_append_sheet(wb, ws, 'SheetJS');
        ws['!cols'] = [
          {width: 5},
          {width: 20},
          {width: 25},
          {width: 20},
          {width: 25},
          {width: 20},
        ];

        // ws.A1.s = {fill: {fgColor: {rgb: 'ff6600'}}};
        /* write file */
        const wbout = await XLSX.write(wb, {type: 'binary', bookType: 'xlsx'});
        // const file = 'file://' + RNFS.ExternalStorageDirectoryPath + '/cihuy.xlsx';
        const file = RNFS.DownloadDirectoryPath + '/' + `report_${date}.xlsx`;
        await writeFile(file, wbout, 'ascii')
          .then(async (res) => {
            const excelFile = await RNFS.readFile(file, 'ascii');
            // console.log('filee', file);

            const workbook = await XLSX.read(excelFile, {type: 'binary'});
            // console.log(workbook, 'excelFile');
            // console.log('tres', res);
            Alert.alert('exportFile success', 'Exported to ' + file);
          })
          .catch((err) => {
            Alert.alert('exportFile Error', 'Error ' + err.message);
          });
      } else {
        console.log('permission denied');
      }
    } catch (err) {
      console.warn(err);
    }
    /* convert AOA back to worksheet */
  };

  const onGetReportDay = async () => {
    const antrian = await firestore().collection('antrian');
    let newAntrian = [];
    const time = await normalizeDate(form.date);
    console.log('time', time);
    await antrian
      .where('createAt', '==', time)
      .get()
      .then(async (snapshot) => {
        const docLength = await snapshot.docs.length;

        // console.log('snapshot', docLength);

        if (docLength > 0) {
          // console.log('snapp', snapshot.docs)
          for (const doc of snapshot.docs) {
            const newData = await doc.data();
            newData.user = await newData.user.get().then((us) => {
              return us.data();
            });
            await newAntrian.push(newData);
          }
        } else {
          alert('Data Tidak Tersedia');
        }
      });

    // console.log('tess', state.antrianList);
    return await newAntrian;
  };

  const onGetReportMonth = async (start, end) => {
    let antrian = await firestore().collection('antrian');
    let newAntrian = [];
    await antrian
      .where('createAt', '>=', start)
      .where('createAt', '<=', end)
      .get()
      .then(async (snapshot) => {
        const docLength = await snapshot.docs.length;

        // console.log('snapshot', docLength);

        if (docLength > 0) {
          // console.log('snapp', snapshot.docs)
          for (const doc of snapshot.docs) {
            const newData = await doc.data();
            newData.user = await newData.user.get().then((us) => {
              return us.data();
            });
            await newAntrian.push(newData);
          }
        } else {
          alert('Data Tidak Tersedia');
        }
      });

    return await newAntrian;
  };

  const normalizeReport = (data) => {
    return data.map((item, index) => ({
      No: index + 1,
      Nama: item.user.nama,
      Email: item.user.email,
      Telephone: item.user.hp,
      Alamat: item.user.alamat,
      Keperluan: item.user.keperluan,
    }));
  };

  const onGetReport = async () => {
    if (form.reportType === 'hari') {
      const dayReport = await onGetReportDay();
      const newData = await normalizeReport(dayReport);

      // console.log('neww data', dayReport);
      if(newData.length > 0) {
        exportExcel(newData, form.date);
      }
    } else {
      const month = form.bulan;

      const years = form.tahun;

      const monthYear = month + '-' + years;

      const startDate = years + '/' + month + '/' + '1';

      const dayMonth = new Date(years, month, 0).getDate();

      const endDate = years + '/' + month + '/' + dayMonth;

      const normsStart = await normalizeDate(startDate);

      const normEnd = await normalizeDate(endDate);

      const montReport = await onGetReportMonth(normsStart, normEnd);

      const newData = await normalizeReport(montReport);

      if(newData.length > 0) {
        exportExcel(newData, monthYear);
      }
      // console.log('timees', montReport, normsStart, normEnd);
    }
  };

  const checkForm = () => {
    if(form.reportType == 'hari') {
      if(form.reportType.length > 0
        && form.date.length > 0
        ) {
        return false;
      }
    }

    if(form.reportType == 'bulan') {
      if(form.reportType.length > 0
        && form.bulan.length > 0
        && form.tahun.length > 0
        ) {
        return false;
      }
    }
    return true;
  }

  return (
    <Container
      style={{
        flex: 1,
        backgroundColor: colors.white,
      }}>
      <Image
        source={require('../../res/image/background.jpg')}
        resizeMode="contain"
        style={{position: 'absolute'}}
      />
      <DeafaultHeader backButton title="Tarik Report Pengunjung" />
      <Content
        style={{
          paddingHorizontal: wp(5),
          paddingTop: hp(2),
        }}>
        <View
          style={{marginBottom: wp(2), alignItems: 'center', width: wp(90)}}>
          <Image
            style={{
              flex: 1,
              width: wp(25),
              height: wp(25),
              resizeMode: 'contain',
            }}
            source={require('../../res/image/report.png')}
          />
        </View>
        <View style={{marginBottom: wp(2), width: wp(90)}}>
          <RePicker
            label="Pilih Jenis Report"
            items={[
              {value: 'hari', label: 'Harian'},
              {value: 'bulan', label: 'Bulanan'},
            ]}
            selectedValue={form.reportType}
            onValueChange={(value) => {
              updateForm({
                reportType: value,
              });
            }}
          />
        </View>
        {form.reportType === 'hari' ? (
          <View style={{marginVertical: wp(2), width: wp(90)}}>
            <ReDatePicker
              placeholder="Tanggal Kunjungan"
              selectedDate={form.date}
              onDateChange={(date) => {
                updateForm({date: date});

                // console.log('Date', normalizeDate(date));
              }}
            />
          </View>
        ) : form.reportType === 'bulan' ? (
          <View>
            <View style={{marginVertical: wp(2), width: wp(90)}}>
              <RePicker
                label="Pilih Bulan"
                items={bulan}
                selectedValue={form.bulan}
                onValueChange={(value) => {
                  updateForm({
                    bulan: value,
                  });
                }}
              />
            </View>
            <View style={{marginVertical: wp(2), width: wp(90)}}>
              <RePicker
                label="Pilih Tahun"
                items={year}
                selectedValue={form.tahun}
                onValueChange={(value) => {
                  updateForm({
                    tahun: value,
                  });
                }}
              />
            </View>
          </View>
        ) : null}

        <View style={{marginVertical: wp(2), marginTop: wp(4), width: wp(90)}}>
          <ReButton
            label="Tarik Report"
            disabled={checkForm()}
            onPress={async () => {
              onGetReport();
            }}
          />
        </View>
        <ReLoadingModal
          visible={state.loading}
          title="Menambahkan Fasilitas .."
        />

        <View style={{height: wp(10)}} />
      </Content>
    </Container>
  );
};

export default ReportPage;
