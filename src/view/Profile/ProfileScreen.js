/* eslint-disable no-alert */
/* eslint-disable react-native/no-inline-styles */
import React, {useState, useEffect} from 'react';
import {Alert, View, Image, AsyncStorage, TouchableOpacity} from 'react-native';
import {Container, Content, Text} from 'native-base';
import DeafaultHeader from '../../component/DeafaultHeader';
import SideBar from '../../component/SideBar';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import colors from '../../res/colors/index';
import {useNavigation} from '@react-navigation/native';

import ReTextInput from '../../component/ReTextInput';
import RePicker from '../../component/RePickerCustom';
import ReButton from '../../component/ReButton';

import {country} from '../../Services/data';

import firestore from '@react-native-firebase/firestore';
import storage from '@react-native-firebase/storage';
import uuid from 'react-native-uuid';

import ReModalImagePicker from '../../component/ReModalImagePicker';

const initialForm = {
  id: '',
  nama: '',
  email: '',
  hp: '',
  password: '',
  username: '',
  foto: '',
  fotoObject: {},

  ktp: '',
  ktpObject: {},
  negara: {value: 'ID', label: 'Indonesia'},
  gender: {},
  keperluan: '',
  alamat: '',
  role: '',
};

const genderList = [
  {value: 'L', label: 'Laki-Laki'},
  {value: 'L', label: 'Perempuan'},
];

const ProfileScreen = () => {
  const [showSideBar, setShowSide] = useState(false);
  const navigation = useNavigation();

  const [state, setState] = useState({
    disableForm: false,
    loading: false,
    loadingPopup: false,
    visiblePicker: false,
  });

  const updateLocalState = (newData) => {
    setState((prev) => ({...prev, ...newData}));
  };
  const [form, setForm] = useState(initialForm);

  const updateForm = (newData) => {
    setForm((prev) => ({
      ...prev,
      ...newData,
    }));
  };

  const onLogout = async () => {
    await AsyncStorage.clear();
    navigation.reset({
      index: 0,
      routes: [{name: 'LoginScreen'}],
    });
    navigation.navigate('LoginScreen');
  };

  const logoutAlert = () => {
    Alert.alert('Logout!', 'Apakah Kamu yakin ingin Logout ?', [
      {
        text: 'Cancel',
        onPress: () => null,
        style: 'cancel',
      },
      {text: 'YES', onPress: () => onLogout()},
    ]);
  };

  useEffect(() => {
    AsyncStorage.getItem('userData').then((item) => {
      const user = JSON.parse(item);
      if (user && Object.keys(user).length > 0) {
        const gender = genderList.find((v) => v.label === user.gender);

        const negara = country.find((v) => v.label === user.negara);

        updateForm({
          id: user.id,
          nama: user.nama,
          email: user.email,
          hp: user.hp,
          alamat: user.alamat,
          gender: gender,
          negara: negara,
          keperluan: user.keperluan,
          foto: user.foto,
          ktp: user.ktp,
          username: user.username,
          password: user.password,
          role: user.role,
        });
      }
      // console.log('parse', parse);
    });
  }, [state.loadingPopup]);

  const togglePicker = () => {
    updateLocalState({visiblePicker: !state.visiblePicker});
  };

  const preparedFoto = async (data) => {
    const uri = data.foto;
    const ext = uri.split('.').pop();
    const filename = `${uuid.v1()}.${ext}`;
    const imageRef = storage().ref(`foto/${filename}`);
    await imageRef.putFile(uri);
    const url = await imageRef.getDownloadURL();

    return url;
  };

  const preparedKtp = async (data) => {
    const uri = data.ktp;
    const ext = uri.split('.').pop();
    const filename = `${uuid.v1()}.${ext}`;
    const imageRef = storage().ref(`foto/${filename}`);
    await imageRef.putFile(uri);
    const url = await imageRef.getDownloadURL();
    return url;
  };

  const preparedData = async () => {
    const formList = form;

    const fotoUri =
      (await Object.keys(formList.fotoObject).length) > 0
        ? await preparedFoto(formList)
        : formList.foto;
    const ktpUri =
      (await Object.keys(formList.ktpObject).length) > 0
        ? await preparedKtp(formList)
        : formList.ktp;

    return {
      id: formList.id,
      data: {
        nama: formList.nama,
        email: formList.email,
        hp: formList.hp,
        alamat: formList.alamat,
        gender: formList.gender.label,
        negara: formList.negara.label,
        keperluan: formList.keperluan,
        // role: 'user',
        foto: fotoUri,
        ktp: ktpUri,
        username: formList.username,
        password: formList.password,
        createAt: firestore.FieldValue.serverTimestamp(),
        role: formList.role,
      },
    };
  };

  const post = firestore().collection('user');

  const onSaveUser = async () => {
    await updateLocalState({loadingPopup: true});
    const formList = await preparedData();

    console.log('form', formList);
    await post
      .doc(formList.id)
      .update({
        ...formList.data,
      })
      .then(() => {
        alert('Berhasil menyimpan data');
        // navigation.goBack();

        const newwlist = {
          id: formList.id,
          ...formList.data,
        };
        AsyncStorage.setItem('userData', JSON.stringify(newwlist));
        setState({
          ...state,
          disableForm: !state.disableForm,
        });
      })
      .catch(() => {
        alert('Gagal menyimpan data');
      });

    await updateLocalState({loadingPopup: false});
  };

  const checkFormRegister = () => {
    if (
      state.disableForm &&
      form.email.length > 0 &&
      form.password.length > 0 &&
      form.username.length > 0
    ) {
      return false;
    }
    return true;
  };

  return (
    <Container
      style={{
        flex: 1,
        backgroundColor: colors.white,
      }}>
      <Image
        source={require('../../res/image/background.jpg')}
        resizeMode="contain"
        style={{position: 'absolute'}}
      />
      <DeafaultHeader
        backButton
        title="Profile"
        centerTitle
        rightButton={state.disableForm ? 'close' : 'pencil'}
        onRightButtonPress={() => {
          console.log('jalan');
          setState({
            ...state,
            disableForm: !state.disableForm,
          });
        }}
      />
      {state.disableForm ? (
        <Content style={{}}>
          <View
            style={{
              margin: wp(4),
              paddingHorizontal: wp(5),
              alignItems: 'center',
              justifyContent: 'center',
              flexDirection: 'row',
            }}>
            <View style={{flexDirection: 'row', marginBottom: wp(2)}}>
              <View style={{marginVertical: hp(1.2), marginHorizontal: wp(10)}}>
                <TouchableOpacity
                  onPress={() => {
                    togglePicker();
                    updateLocalState({
                      selectedImage: 'foto',
                    });
                  }}
                  style={{justifyContent: 'center', alignItems: 'center'}}
                  transparent>
                  {form.foto.length > 0 ? (
                    <Image
                      source={{
                        uri: form.foto,
                      }}
                      style={{
                        height: hp(10),
                        width: hp(10),
                        borderRadius: hp(20),
                        resizeMode: 'cover',
                      }}
                    />
                  ) : (
                    <Image
                      source={require('../../res/image/Profile.png')}
                      style={{
                        height: hp(10),
                        width: hp(10),
                        borderRadius: hp('20%'),
                        resizeMode: 'cover',
                      }}
                    />
                  )}
                </TouchableOpacity>
                <View style={{alignItems: 'center'}}>
                  <Text style={{fontSize: wp(3)}}>Foto Profile</Text>
                </View>
              </View>
              <View style={{marginVertical: hp(1.2), marginHorizontal: wp(10)}}>
                <TouchableOpacity
                  onPress={() => {
                    togglePicker();
                    updateLocalState({
                      selectedImage: 'ktp',
                    });
                  }}
                  style={{justifyContent: 'center', alignItems: 'center'}}
                  transparent>
                  {form.ktp.length > 0 ? (
                    <Image
                      source={{
                        uri: form.ktp,
                      }}
                      style={{
                        height: hp(10),
                        width: hp(10),
                        borderRadius: hp(20),
                        resizeMode: 'cover',
                      }}
                    />
                  ) : (
                    <Image
                      source={require('../../res/image/credit-card.png')}
                      style={{
                        height: hp(10),
                        width: hp(10),
                        // borderRadius: hp('20%'),
                        resizeMode: 'cover',
                      }}
                    />
                  )}
                </TouchableOpacity>
                <View style={{alignItems: 'center'}}>
                  <Text style={{fontSize: wp(3)}}>Foto KTP</Text>
                </View>
              </View>
            </View>
          </View>
          <View
            style={{
              // marginVertical: wp(2),
              // borderWidth: 1,
              // paddingVertical: wp(2),
              paddingHorizontal: wp(4),
            }}>
            <View style={{marginVertical: wp(1), width: wp(90)}}>
              <ReTextInput
                label="Nama"
                value={form.nama}
                labelColor={colors.gray11}
                valueColor={colors.green02}
                onChangeText={(val) => {
                  updateForm({nama: val});
                }}
              />
            </View>
            <View style={{marginVertical: wp(1), width: wp(90)}}>
              <ReTextInput
                label="Email"
                value={form.email}
                labelColor={colors.gray11}
                valueColor={colors.green02}
                onChangeText={(val) => {
                  updateForm({email: val});
                }}
              />
            </View>
            <View style={{marginVertical: wp(1), width: wp(90)}}>
              <ReTextInput
                label="No. HP"
                value={form.hp}
                labelColor={colors.gray11}
                valueColor={colors.green02}
                onChangeText={(val) => {
                  updateForm({hp: val});
                }}
              />
            </View>
            <View style={{marginVertical: wp(1), width: wp(90)}}>
              <ReTextInput
                label="Alamat"
                value={form.alamat}
                labelColor={colors.gray11}
                valueColor={colors.green02}
                onChangeText={(val) => {
                  updateForm({alamat: val});
                }}
              />
            </View>
            <View style={{marginVertical: wp(1), width: wp(90)}}>
              <RePicker
                label="Jenis Kelamin"
                labelColor={colors.gray11}
                valueColor={colors.green02}
                data={genderList}
                value={form.gender}
                onValueChange={(value) => {
                  updateForm({gender: value});
                  console.log('val', value);
                }}
              />
            </View>

            <View style={{marginVertical: wp(1), width: wp(90)}}>
              <RePicker
                label="Negara"
                labelColor={colors.gray11}
                valueColor={colors.green02}
                data={country}
                value={form.negara}
                onValueChange={(value) => {
                  updateForm({negara: value});
                }}
              />
            </View>
            <View style={{marginVertical: wp(1), width: wp(90)}}>
              <ReTextInput
                label="Keperluan Kedatangan"
                labelColor={colors.gray11}
                valueColor={colors.green02}
                value={form.keperluan}
                onChangeText={(val) => {
                  updateForm({keperluan: val});
                }}
              />
            </View>
            <View style={{marginVertical: wp(1), width: wp(90)}}>
              <ReTextInput
                label="Username"
                labelColor={colors.gray11}
                valueColor={colors.green02}
                value={form.username}
                onChangeText={(val) => {
                  updateForm({username: val});
                }}
              />
            </View>
            <View style={{marginVertical: wp(1), width: wp(90)}}>
              <ReTextInput
                label="Password"
                labelColor={colors.gray11}
                valueColor={colors.green02}
                value={form.password}
                onChangeText={(val) => {
                  updateForm({password: val});
                }}
                passwordInput
              />
            </View>
            <View
              style={{marginVertical: wp(2), marginTop: wp(4), width: wp(90)}}>
              <ReButton
                label="Simpan"
                disabled={checkFormRegister()}
                onPress={() => {
                  onSaveUser();
                }}
              />
            </View>
          </View>
          <View style={{height: wp(10)}} />
        </Content>
      ) : (
        <Content style={{}}>
          <View
            style={{
              marginVertical: wp(6),
              marginHorizontal: wp(4),
              paddingHorizontal: wp(5),
              alignItems: 'center',
              // borderWidth: 1,
              paddingVertical: wp(4),
              flexDirection: 'row',
              borderRadius: wp(2),
              backgroundColor: 'white',
              // shadowColor: '#000',
              // shadowOffset: {
              //   width: 0,
              //   height: 2,
              // },
              // shadowOpacity: 0.25,
              // shadowRadius: 3.84,

              // elevation: 5,
            }}>
            <View style={{flex: 1, alignItems: 'center'}}>
              <TouchableOpacity>
                <Image
                  source={{
                    uri: form.foto,
                  }}
                  style={{
                    height: hp(10),
                    width: hp(10),
                    borderRadius: hp(20),
                    resizeMode: 'cover',
                  }}
                />
              </TouchableOpacity>

              <Text style={{fontSize: wp(4)}}>Foto Profile</Text>
            </View>
            <View style={{flex: 1, alignItems: 'center'}}>
              <TouchableOpacity>
                <Image
                  source={{
                    uri: form.ktp,
                  }}
                  style={{
                    height: hp(10),
                    width: hp(10),
                    borderRadius: hp(20),
                    resizeMode: 'cover',
                  }}
                />
              </TouchableOpacity>
              <Text style={{fontSize: wp(4)}}>Foto KTP</Text>
            </View>
          </View>
          <View
            style={{
              // marginVertical: wp(3),
              marginHorizontal: wp(4),
              paddingHorizontal: wp(5),
              // alignItems: 'center',
              // borderWidth: 1,
              paddingVertical: wp(4),
              // flexDirection: 'row',
              borderRadius: wp(2),
              backgroundColor: 'rgba(255,255,255, 0.5)',
              // shadowColor: '#000',
              // shadowOffset: {
              //   width: 0,
              //   height: 2,
              // },
              // shadowOpacity: 0.25,
              // shadowRadius: 3.84,

              // elevation: 5,
            }}>
            <View style={{marginVertical: wp(1)}}>
              <Text style={{fontSize: wp(3)}}>Nama</Text>
              <Text style={{fontSize: wp(4), fontWeight: 'bold'}}>
                {form.nama}
              </Text>
            </View>
            <View style={{marginVertical: wp(1)}}>
              <Text style={{fontSize: wp(3)}}>Email</Text>
              <Text style={{fontSize: wp(4), fontWeight: 'bold'}}>
                {form.email}
              </Text>
            </View>
            <View style={{marginVertical: wp(1)}}>
              <Text style={{fontSize: wp(3)}}>No Hp</Text>
              <Text style={{fontSize: wp(4), fontWeight: 'bold'}}>
                {form.hp}
              </Text>
            </View>
            <View style={{marginVertical: wp(1)}}>
              <Text style={{fontSize: wp(3)}}>Jenis Kelamin</Text>
              <Text style={{fontSize: wp(4), fontWeight: 'bold'}}>
                {form.gender.label}
              </Text>
            </View>
            <View style={{marginVertical: wp(1)}}>
              <Text style={{fontSize: wp(3)}}>Asal Negara</Text>
              <Text style={{fontSize: wp(4), fontWeight: 'bold'}}>
                {form.negara.label}
              </Text>
            </View>
            <View style={{marginVertical: wp(1)}}>
              <Text style={{fontSize: wp(3)}}>Alamat</Text>
              <Text style={{fontSize: wp(4), fontWeight: 'bold'}}>
                {form.alamat}
              </Text>
            </View>
          </View>
          <View
            style={{
              marginVertical: wp(4),
              alignSelf: 'center',
              marginHorizontal: wp(4),
              width: wp(90),
            }}>
            <ReButton
              label="Logout"
              // disabled={checkFormRegister()}
              backgroundColor={colors.warning}
              onPress={() => {
                logoutAlert();
              }}
            />
          </View>
          <View style={{height: wp(10)}} />
        </Content>
      )}
      <SideBar
        visibleModal={showSideBar}
        onBackButtonPress={() => setShowSide(!showSideBar)}
      />
      <ReModalImagePicker
        visible={state.visiblePicker}
        onCancel={() => togglePicker()}
        onPickImage={(image) => {
          togglePicker();
          if (state.selectedImage === 'foto') {
            updateForm({
              fotoObject: image,
              foto: image.uri,
            });
          } else {
            updateForm({
              ktpObject: image,
              ktp: image.uri,
            });
          }
        }}
      />
    </Container>
  );
};

export default ProfileScreen;
