/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable react-native/no-inline-styles */
import React, {useState, useEffect} from 'react';
import {
  View,
  Image,
  TouchableOpacity,
  ActivityIndicator,
  AsyncStorage,
  ImageBackground,
  BackHandler,
  Alert,
} from 'react-native';

import {Container, Text, Content} from 'native-base';

import {useNavigation, useFocusEffect} from '@react-navigation/native';

import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';

import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import colors from '../../res/colors/index';
import LinearGradient from 'react-native-linear-gradient';
import DeafaultHeader from '../../component/DeafaultHeader';

import firestore from '@react-native-firebase/firestore';

const HomeUser = () => {
  const navigation = useNavigation();

  const [state, setState] = useState({
    nama: '',
    loading: false,
    antrian: '-',
  });

  const [wisata, setWisata] = useState([]);

  const updateLocalState = (newData) => {
    setState((prev) => ({...prev, ...newData}));
  };

  const fasilitas = firestore().collection('fasilitas');

  const loadUserList = async () => {
    await updateLocalState({loading: true});

    await fasilitas.orderBy('createAt', 'desc').onSnapshot((snapshot) => {
      let list = [];
      // console.log('snapshot', snapshot);
      snapshot.forEach((doc) => {
        list.push({
          id: doc.id,
          foto: doc.data().foto,
          judul: doc.data().judul,
          deskripsi: doc.data().deskripsi,
          createAt: doc.data().createAt,
        });
      });
      setWisata(list);
    });

    await updateLocalState({loading: false});
  };

  const getAntrian = () => {
    AsyncStorage.getItem('antrian').then((item) => {
      if (item) {
        console.log('get antri', item);
        updateLocalState({
          antrian: item,
        });
      }
    });
  };

  useEffect(() => {
    AsyncStorage.getItem('userData').then((item) => {
      const user = JSON.parse(item);
      if (user && Object.keys(user).length > 0) {
        updateLocalState({
          nama: user.nama,
        });
        // postLoginHistory(user);
      }
      // console.log('parse', parse);
    });
    getAntrian();
  }, []);

  useEffect(() => {
    loadUserList();
  }, []);

  useFocusEffect(
    React.useCallback(() => {
      const onBackPress = () => {
        Alert.alert('Exit App!', 'Apakah Kamu yakin ingin keluar Aplikasi?', [
          {
            text: 'Cancel',
            onPress: () => {
              // return true;
            },
            style: 'cancel',
          },
          {
            text: 'YES',
            onPress: () => {
              BackHandler.exitApp();
              // return false;
            },
          },
        ]);
        return true;
      };

      BackHandler.addEventListener('hardwareBackPress', onBackPress);

      return () =>
        BackHandler.removeEventListener('hardwareBackPress', onBackPress);
    }, []),
  );

  return (
    <Container
      style={{
        flex: 1,
        backgroundColor: colors.white,
      }}>
      <Image
        source={require('../../res/image/background.jpg')}
        resizeMode="contain"
        style={{position: 'absolute'}}
      />
      <ImageBackground
        style={{flex: 1, paddingTop: hp(1.3)}}
        // source={require('../../res/image/BGdashboard.jpg')}
        // source={require('../../res/image/background.jpg')}
      >
        <DeafaultHeader
          title={`Selamat Datang, ${state.nama}`}
          rightButton="user"
          righButtonSize={hp(4)}
          onRightButtonPress={() => {
            navigation.navigate('ProfileScreen');
          }}
        />
        <View
          style={{
            paddingHorizontal: wp(5),
            marginBottom: hp(3),
            marginTop: hp(-1.5),
          }}>
          <Text style={{fontSize: wp(4.2), fontWeight: 'bold'}}>
            {`Nomor antrian : ${state.antrian}`}
          </Text>
        </View>
        <View style={{paddingHorizontal: wp(5), marginTop: hp(-1)}}>
          <View style={{marginTop: wp(0)}}>
            <Text style={{fontSize: wp(4.2), fontStyle: 'italic'}}>
              Berbagai Wisata Menarik Wasur
            </Text>
          </View>
        </View>
        <View
          style={{
            flexDirection: 'row',
            marginTop: wp(2),
            marginHorizontal: wp(5),
            alignItems: 'center',
          }}>
          <FontAwesome5 name="home" color={colors.primarydark} size={wp(4.5)} />
          <Text
            style={{
              marginLeft: wp(2),
              fontSize: wp(4.5),
              fontWeight: 'bold',
              color: colors.lightBlack,
            }}>
            Wisata
          </Text>
        </View>
        <Content>
          {state.loading ? (
            <View style={{paddingVertical: hp('2%')}}>
              <ActivityIndicator size={'large'} />
            </View>
          ) : (
            <View
              style={{
                // marginVertical: wp(4),
                marginHorizontal: wp(5),
                // paddingHorizontal: wp(5),
                alignItems: 'center',
                // borderWidth: 1,
                paddingVertical: wp(2),
              }}>
              {wisata.map((data, index) => (
                <LinearGradient
                  key={index}
                  colors={[
                    'rgba(18, 145, 136, 0.9)',
                    'rgba(255, 255, 255, 0.5)',
                  ]}
                  style={{
                    // flexDirection: 'row',
                    marginBottom: wp(2),
                    width: '100%',
                    paddingVertical: hp(2),
                    paddingHorizontal: wp(3),
                    borderRadius: wp(1.5),
                  }}>
                  <TouchableOpacity
                    style={{flexDirection: 'row'}}
                    onPress={() => {
                      navigation.navigate('DetailWisata', {data: data});
                    }}>
                    <View style={{flex: 1, marginRight: wp(2)}}>
                      <Image
                        source={{uri: data.foto}}
                        style={{
                          height: hp(15),
                          width: hp(15),
                          borderRadius: wp(1),
                          resizeMode: 'cover',
                        }}
                      />
                    </View>
                    <View style={{flex: 2}}>
                      <View>
                        <Text
                          numberOfLines={2}
                          ellipsizeMode="tail"
                          style={{fontSize: wp(3.8), fontWeight: 'bold'}}>
                          {data.judul}
                        </Text>
                      </View>
                      <View>
                        <Text
                          numberOfLines={4}
                          ellipsizeMode="tail"
                          style={{fontSize: wp(3.5)}}>
                          {data.deskripsi}
                        </Text>
                      </View>
                    </View>
                  </TouchableOpacity>
                </LinearGradient>
              ))}
            </View>
          )}
        </Content>
      </ImageBackground>
    </Container>
  );
};

export default HomeUser;
