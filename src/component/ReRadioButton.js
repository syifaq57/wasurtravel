/* eslint-disable react-native/no-inline-styles */
import React, {useState} from 'react';
import {View, TextInput, TouchableOpacity} from 'react-native';
import {Text} from 'native-base';
import colors from '../res/colors/index';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

const ReRadioButton = (props) => {
  const [radioSelected, setRadioSelected] = useState(null);
  return (
    <View
      style={{
        backgroundColor: colors.white,
        borderWidth: 1,
        borderRadius: wp(1),
        borderColor: colors.gray11,
        padding: wp(2),
      }}>
      <View>
        <View style={{paddingLeft: wp(0.4), marginBottom: hp(1.8)}}>
          <Text
            style={{
              fontSize: wp(3),
              color: colors.lightBlack,
              fontWeight: 'bold',
            }}>
            {props.label.toUpperCase()}
          </Text>
        </View>
        {props.items.map((item, index) => (
          <View
            key={index}
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              marginBottom: wp(2),
            }}>
            <TouchableOpacity
              style={{
                width: wp(5),
                height: wp(5),
                borderWidth: 2,
                borderColor: colors.gray11,
                borderRadius: wp(10),
                marginRight: wp(2),
                padding: wp(0.1),
                justifyContent: 'center',
                alignItems: 'center',
              }}
              onPress={() => {
                setRadioSelected(index);
                props.onChangeRadio(item);
              }}>
              <View
                style={{
                  borderRadius: wp(10),
                  width: wp(3.5),
                  height: wp(3.5),
                  backgroundColor:
                    index === radioSelected
                      ? colors.greenDefault
                      : colors.white,
                }}
              />
            </TouchableOpacity>
            <Text style={{fontSize: wp(4)}}>{item.label}</Text>
          </View>
        ))}
      </View>
    </View>
  );
};

export default ReRadioButton;
