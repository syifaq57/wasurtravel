/* eslint-disable react-native/no-inline-styles */
import React, {useState} from 'react';
import {View, TouchableOpacity, Image, Linking} from 'react-native';
import {Text} from 'native-base';
import colors from '../res/colors/index';
import Modal from 'react-native-modal';
// import Rate, {AndroidMarket} from 'react-native-rate';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

const onRate = () => {
  var url = 'https://play.google.com/store/apps/developer?id=UCP+Studio';
  Linking.openURL(url)
    .then(() => {
      console.log('WhatsApp Opened');
    })
    .catch(() => {
      alert('Make sure Whatsapp installed on your device');
    });
};

const openWA = () => {
  var msg = 'Hallo, saya butuh jasa pembuatan aplikasi';
  var tlp = '6281246579132';
  var url = 'whatsapp://send?text=' + msg + '&phone=' + tlp;
  Linking.openURL(url)
    .then(() => {
      console.log('WhatsApp Opened');
    })
    .catch(() => {
      alert('Make sure Whatsapp installed on your device');
    });
};

const SideBar = (props) => {
  const [localState] = useState({
    title: 'UCP Studio',
  });
  return (
    <View>
      <Modal
        isVisible={props.visibleModal}
        style={{marginVertical: 0, marginHorizontal: 0}}
        backdropOpacity={0.3}
        animationIn={'slideInLeft'}
        animationOut={'slideOutLeft'}
        onBackdropPress={props.onBackButtonPress}
        onBackButtonPress={props.onBackButtonPress}>
        <View
          style={{
            backgroundColor: 'white',
            height: '100%',
            alignItems: 'center',
            paddingTop: hp(10),
          }}>
          <View style={{alignItems: 'center'}}>
            <Image
              source={require('../res/image/ucp.png')}
              style={{
                width: wp(45),
                height: wp(45),
                resizeMode: 'contain',
                marginBottom: hp('-4%'),
              }}
            />
          </View>
          <View style={{alignItems: 'center'}}>
            {/* <Text
              style={{
                fontSize: wp('7%'),
                textAlign: 'center',
                fontWeight: 'bold',
              }}>
              {localState.title}
            </Text> */}
            {/* <Text style={{fontSize: wp('4%')}}>2020</Text> */}
          </View>
          <TouchableOpacity
            onPress={() => onRate()}
            style={{
              paddingVertical: wp('3.5%'),
              paddingHorizontal: hp('3%'),
              minWidth: wp('50%'),
              marginTop: hp('5%'),
              borderRadius: wp('2%'),
              backgroundColor: colors.gray092,
              alignItems: 'center',
            }}>
            <Text style={{color: 'white', fontSize: wp('4.5%')}}>
              Other Apps
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => openWA()}
            style={{
              paddingVertical: wp('3.5%'),
              paddingHorizontal: hp('3%'),
              minWidth: wp('50%'),
              marginTop: hp('5%'),
              borderRadius: wp('2%'),
              backgroundColor: colors.gray092,
              alignItems: 'center',
            }}>
            <Text style={{color: 'white', fontSize: wp('4.5%')}}>
              Contact Developer
            </Text>
          </TouchableOpacity>
          <View style={{position: 'absolute', bottom: 10}}>
            <Text style={{fontSize: wp('4%')}}>Created BY @UCPStudio</Text>
          </View>
        </View>
      </Modal>
    </View>
  );
};

export default SideBar;
