/* eslint-disable react-native/no-inline-styles */
import React, {useState} from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import {Picker} from 'native-base';
import colors from '../res/colors';
import Modal from 'react-native-modal';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {ScrollView} from 'react-native-gesture-handler';
import FontAweSome from 'react-native-vector-icons/FontAwesome';

const RePicker = (props) => {
  const [selectedValue, setSelectedValue] = useState(props.value || {});
  const [showList, setShowList] = useState(false);

  const toggleShowList = () => {
    setShowList(!showList);
  };

  return (
    <View
      pointerEvents={props.disabled ? 'none' : 'auto'}
      style={{
        borderBottomWidth: 2,
        borderColor: colors.black,
      }}>
      <TouchableOpacity
        style={{
          minHeight: hp(7.8),
          flexDirection: 'row',
          alignItems: 'center',
        }}
        onPress={() => toggleShowList()}>
        <Text
          style={{
            position: 'absolute',
            top: Object.keys(selectedValue).length > 0 ? hp(-0.3) : hp(2),
            marginLeft:
              Object.keys(selectedValue).length > 0 ? wp(0.5) : wp(2.5),
            fontSize: Object.keys(selectedValue).length > 0 ? wp(2.8) : wp(4),
            fontFamily: 'Poppins-Regular',
            color:
              Object.keys(selectedValue).length > 0
                ? colors.gray08
                : colors.white,
          }}>
          {props.label}
        </Text>
        {Object.keys(selectedValue).length > 0 ? (
          <View
            style={{
              paddingLeft: wp(2),
              marginTop: hp(1.3),
              marginBottom: hp(-0.6),
            }}>
            <Text
              style={{
                fontSize: wp(4),
                fontFamily: 'Poppins-Regular',
                color: props.valueColor ? props.valueColor : colors.white,
              }}>
              {selectedValue.label}
            </Text>
          </View>
        ) : null}
        <View style={{position: 'absolute', right: wp(4)}}>
          <FontAweSome name="chevron-down" color={colors.black} size={wp(4)} />
        </View>
      </TouchableOpacity>

      <Modal
        animationIn={'fadeIn'}
        isVisible={showList}
        onBackdropPress={() => toggleShowList()}
        backdropOpacity={0.5}
        animationOut={'fadeOut'}
        animationInTiming={500}>
        <View
          style={{
            // position: 'absolute',
            maxHeight: hp(55),
            // minHeight: hp(30),
            // borderRadius: wp(2),
          }}>
          <ScrollView style={{backgroundColor: 'white'}}>
            {props.data.map((data, index) => (
              <TouchableOpacity
                onPress={() => {
                  setSelectedValue(data);
                  props.onValueChange(data);
                  toggleShowList();
                }}
                key={index}
                style={{
                  backgroundColor:
                    index % 2 === 0 ? colors.soft5 : colors.soft4,
                  // borderRadius: wp(2),

                  paddingVertical: hp(2),
                  paddingHorizontal: wp(3),
                  // borderBottomWidth: 0.5,
                  flexDirection: 'row',
                }}>
                <View style={{justifyContent: 'center', marginRight: wp(2)}}>
                  <FontAweSome
                    name="chain"
                    color={colors.gray10}
                    size={wp(3)}
                  />
                </View>
                <View>
                  <Text
                    style={{
                      fontSize: hp(2.8),
                      fontFamily: 'JosefinSans-Bold',
                      color: colors.gray10,
                    }}>
                    {data.label}
                  </Text>
                </View>
              </TouchableOpacity>
            ))}
          </ScrollView>
        </View>
      </Modal>
    </View>
  );
};

export default RePicker;
