import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';

import LoginScreen from './view/Login/LoginScreen';
import RegisterScreen from './view/Register/RegisterScreen';

import HomeAdmin from './view/Admin/HomeAdmin';
import TambahFasilitas from './view/Admin/TambahFasilitas';
import UserList from './view/Admin/UserList';

import ProfileScreen from './view/Profile/ProfileScreen';

import UserEdit from './view/Admin/UserEdit';
import HomeUser from './view/User/HomeUser';
import DetailWisata from './view/User/DetailWisata';
import ReportPage from './view/Admin/ReportPage';
import SplashScreen from './view/SplashScreen/SplashScreen';

const Stack = createStackNavigator();

const Route = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator
        initialRouteName="SplashScreen"
        screenOptions={{headerShown: false}}>
        <Stack.Screen name="SplashScreen" component={SplashScreen} />
        <Stack.Screen name="LoginScreen" component={LoginScreen} />
        <Stack.Screen name="HomeAdmin" component={HomeAdmin} />
        <Stack.Screen name="HomeUser" component={HomeUser} />
        <Stack.Screen name="RegisterScreen" component={RegisterScreen} />
        <Stack.Screen name="ProfileScreen" component={ProfileScreen} />
        <Stack.Screen name="TambahFasilitas" component={TambahFasilitas} />
        <Stack.Screen name="ReportPage" component={ReportPage} />
        <Stack.Screen name="UserList" component={UserList} />
        <Stack.Screen name="UserEdit" component={UserEdit} />
        <Stack.Screen name="DetailWisata" component={DetailWisata} />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default Route;
